

function initialize() {
    var mapProp = {
        center: new google.maps.LatLng(43.350017, 17.7965431),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false
    };

    var image = {
        url: 'css/images/mapa1.png',
        size: new google.maps.Size(164, 124)
    };

    var map = new google.maps.Map(document.getElementById("contact-map"), mapProp);

    var marker = new google.maps.Marker({
        map: map,
        position: mapProp.center,
        animation: google.maps.Animation.BOUNCE,
        title: "NSoft, Bleiburških žrtava, Mostar",
        icon: image,
        scrollwheel: mapProp.scrollwheel
    });
}

google.maps.event.addDomListener(window, 'load', initialize);


///
// Set active class on navigation element related to scroll position
function setActiveClass(position, elemPosion, elems) {
	 if (position < elemPosion.about) {
        elems[0].classList.add('active');
    } else if (position >= elemPosion.about && position < elemPosion.helpDownload) {
        elems[1].classList.add('active');
     }   else if (position >= elemPosion.helpDownload && position < elemPosion.contact) {
        elems[2].classList.add('active');
    } else if (position >= elemPosion.contact) {
        elems[3].classList.add('active');
    }

}

// Remove active class from elements
function removeActiveClass(items) {
    var len = items.length,
        i;

    for (i = 0; i < len; i++) {
        items[i].classList.remove('active');
    }
}

// Store navigation element

var nav = document.getElementsByClassName('main-menu')[0].children[0],
    navLinks = nav.getElementsByTagName('a');
   


var navElemsPosition = {
    applogo: 0 ,
    about: 1000 ,
    helpDownload: 2000,
    contact: 2800 
}



// Bind click events on navigation elements



Object.keys(navLinks).forEach(function (key) {
    var elem = navLinks[key];
     

    elem.addEventListener('click', function (e) {
        var hash = e.target.hash,
            position;

      

        removeActiveClass(navLinks); // Remove active class
        this.classList.add('active'); // Add active class

       
});

});

// Update navigation active class on scroll position
// function for scrol

var scrollPos = window.scrollY,
    timeout;
  
setActiveClass(scrollPos, navElemsPosition, navLinks); // Set active class
window.addEventListener('scroll', function (e) {
    clearTimeout(timeout);

    timeout = setTimeout(function () {
        scrollPos = window.scrollY; // Update scroll position

        removeActiveClass(navLinks); // Remove active class
        setActiveClass(scrollPos, navElemsPosition, navLinks); // Set active class
    }, 150);
});


//*********************************************************************************************

// Update navigation active class on scroll position

//***********************************************************************************************




/*  II način validacije forme
var x= document.forms["form"]["subject"].value;



function validateForm() {
	 var y = document.forms["form"]["mail"].value;
    if (y == null || y == "") {
        var f = document.getElementById('em');
        f.placeholder= "Polje nesmije biti prazno";
        f.style.border="1px solid red";
        return false;
           
    } 
    var x = document.forms["form"]["subject"].value;
    if (x == null || x == "") {
    	var s =document.getElementById('subj');
       	s.placeholder = " Polje nesmije biti prazno";
        s.style.border="1px solid red";

        return false;
    
    }
    var x = document.forms["form"]["message"].value;
    if (x == null || x == "") {
    	var t =document.getElementById('text');
    	t.placeholder = " Polje nesmije biti prazno";
        t.style.border="1px solid red";
        return false;
    
    }
};
*/


var x = document.getElementById('read-more');
var y =document.getElementById('poster-text-read');

x.addEventListener('click',more);

function more(){
    y.innerHTML="Amazing Grace... how sweet the sound! GOD'S grace IS amazing, in that it does create great wonder and surprise! How could a Holy God love me? In order to do so it will have to be based on who He is and NOT on who I am.Amazing Grace... how sweet the sound! GOD'S grace IS amazing, in that it does create great wonder and surprise! How could a Holy God love me? In order to do so it will have to be based on who He is and NOT on who I am.";
}


function sendData(form) {
    var xhttp = new XMLHttpRequest();
    var data = '';
    data += 'name=' + encodeURIComponent(form.subject.value);
    data += '&email=' + encodeURIComponent(form.mail.value);
    data += '&message=' + encodeURIComponent(form.message.value);

    // If data are sent
    xhttp.onload = function (e) {
        alert(e.target.responseText);
    };

    xhttp.open('POST', '#');
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    xhttp.send(data);
}

var form = document.form;

form.onsubmit = function (e) {
    e.preventDefault();
    var reason = '';
    reason += validateName(form.subject);
    reason += validateEmail(form.mail);
    reason += validateMessage(form.message);


    if (reason.length > 0) {
        alert(reason);
    } else {
        sendData(form);
        // form.submit();

        form.reset();
    }
};

var borderWarning = '1px solid #f00';
var borderNone = 0;
function validateName(name) {
    var error = '';

    if (name.value.trim().length === 0) {
        name.style.border = borderWarning;
        error = 'Prazno name polje!';
    } else {
        name.style.border = borderNone;
        error = '';
    }

    return error;
}

function validateEmail(mail) {
    var error = '';
    var hasSpecialChar = mail.value.indexOf('@');

    if (mail.value.trim().length === 0) {
        mail.style.border = borderWarning;
        error = 'Prazno email polje!';
    } else if (hasSpecialChar === -1) {
        mail.style.border = borderWarning;
        error = 'Missing "@" character!';
    } else if (hasSpecialChar === 0 || hasSpecialChar === mail.value.length - 1) {
        mail.style.border = borderWarning;
        error = 'Pogrešan email!';
    } else {
        mail.style.border = borderNone;
        error = '';
    }

    return error;
}

function validateMessage(message) {
    var error = '';

    if (message.value.trim().length === 0) {
        message.style.border = borderWarning;
        error = 'Prazno message polje!';
    } else {
        message.style.border = borderNone;
        error = '';
    }

    return error;
}
